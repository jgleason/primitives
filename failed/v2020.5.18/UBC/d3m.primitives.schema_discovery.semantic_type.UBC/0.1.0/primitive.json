{
    "id": "b88b7cdd-f07c-4d56-a383-d0c46e2c056b",
    "version": "0.1.0",
    "name": "Semantic Type Inference",
    "description": "A primitive for detecting the semantic type of inputed column data.\n--> Currently Supported: 78 Semantic Types\n --------------------------------------------------------------------------\n |Address        | Code        | Education   | Notes        | Requirement |\n |Affiliate      | Collection  | Elevation   | Operator     | Result      |\n |Age            | Company     | File size   | Organisation | Service     |\n |Affiliation    | Command     | Family      | Order        | Sales       |\n |Album          | Component   | Format      | Origin       | Sex         |\n |Area           | Continent   | Gender      | Owner        | Species     |\n |Artist         | Country     | Genre       | Person       | State       |\n |Birth date     | County      | Grades      | Plays        | Status      |\n |Birth place    | Creator     | Industry    | Position     | Symbol      |\n |Brand          | Credit      | ISBN        | Product      | Team        |\n |Capacity       | Currency    | Jockey      | Publisher    | Team name   |\n |Category       | Day         | Language    | Range        | Type        |\n |City           | Depth       | Location    | Rank         | Weight      |\n |Class          | Description | Manufacturer| Ranking      | Year        |\n |Classification | Director    | Name        | Region       |             |\n |Club           | Duration    | Nationality | Religion     |             |\n --------------------------------------------------------------------------\n\nAttributes\n----------\nmetadata:\n    Primitive's metadata. Available as a class attribute.\nlogger:\n    Primitive's logger. Available as a class attribute.\nhyperparams:\n    Hyperparams passed to the constructor.\nrandom_seed:\n    Random seed passed to the constructor.\ndocker_containers:\n    A dict mapping Docker image keys from primitive's metadata to (named) tuples containing\n    container's address under which the container is accessible by the primitive, and a\n    dict mapping exposed ports to ports on that address.\nvolumes:\n    A dict mapping volume keys from primitive's metadata to file and directory paths\n    where downloaded and extracted files are available to the primitive.\ntemporary_directory:\n    An absolute path to a temporary directory a primitive can use to store any files\n    for the duration of the current pipeline run phase. Directory is automatically\n    cleaned up after the current pipeline run phase finishes.",
    "python_path": "d3m.primitives.schema_discovery.semantic_type.UBC",
    "primitive_family": "SCHEMA_DISCOVERY",
    "algorithm_types": [
        "DATA_CONVERSION"
    ],
    "source": {
        "name": "UBC",
        "contact": "mailto:tonyjos@ubc.cs.ca",
        "uris": [
            "https://github.com/plai-group/ubc_primitives.git"
        ]
    },
    "keywords": [
        "semantic type inference\"",
        "data type detection",
        "data profiler"
    ],
    "installation": [
        {
            "type": "PIP",
            "package_uri": "git+https://github.com/plai-group/ubc_primitives.git@81307862dd7933ab74d396679dbecc8ec30294af#egg=ubc_primitives"
        },
        {
            "type": "FILE",
            "key": "classes_sherlock.npy",
            "file_uri": "https://dl.dropboxusercontent.com/s/k7mjisbfmffw4l4/classes_sherlock.npy?dl=1",
            "file_digest": "0bb18ba9dd97e124c8956f0abb1e8ff3a5aeabe619a3c38852d85ea0ec876c4a"
        },
        {
            "type": "FILE",
            "key": "glove.6B.50d.txt",
            "file_uri": "https://dl.dropboxusercontent.com/s/8x197jze94d82qu/glove.6B.50d.txt?dl=1",
            "file_digest": "d8f717f8dd4b545cb7f418ef9f3d0c3e6e68a6f48b97d32f8b7aae40cb31f96f"
        },
        {
            "type": "TGZ",
            "key": "sherlock_weights",
            "file_uri": "https://dl.dropboxusercontent.com/s/4byt3muredceftm/sherlock_nn.tar.gz?dl=1",
            "file_digest": "1281e208ec884236991df9a49e3c9810edc866fbb0b24fc117f45033c63ec427"
        },
        {
            "type": "TGZ",
            "key": "par_vec_trained_400",
            "file_uri": "https://dl.dropboxusercontent.com/s/yn7n6eso6382ey9/par_vec_trained_400.tar.gz?dl=1",
            "file_digest": "8e7dc7f5876d764761a3093f6ddd315f295a3a6c8578efa078ad27baf08b2569"
        }
    ],
    "schema": "https://metadata.datadrivendiscovery.org/schemas/v0/primitive.json",
    "original_python_path": "primitives_ubc.smi.semantic_type.SemanticTypeInfer",
    "primitive_code": {
        "class_type_arguments": {
            "Inputs": "d3m.container.pandas.DataFrame",
            "Outputs": "d3m.container.pandas.DataFrame",
            "Hyperparams": "primitives_ubc.smi.semantic_type.Hyperparams",
            "Params": "NoneType"
        },
        "interfaces_version": "2020.5.18",
        "interfaces": [
            "transformer.TransformerPrimitiveBase",
            "base.PrimitiveBase"
        ],
        "hyperparams": {
            "use_row_iter": {
                "type": "d3m.metadata.hyperparams.UniformBool",
                "default": false,
                "structural_type": "bool",
                "semantic_types": [
                    "https://metadata.datadrivendiscovery.org/types/ControlParameter"
                ],
                "description": "Whether or not to use row iteration inplace of column interation on dataframe"
            },
            "use_columns": {
                "type": "d3m.metadata.hyperparams.Set",
                "default": [],
                "structural_type": "typing.Sequence[int]",
                "semantic_types": [
                    "https://metadata.datadrivendiscovery.org/types/ControlParameter"
                ],
                "description": "A set of column indices to force primitive to operate on. If any specified column cannot be cast to the type, it is skipped.",
                "elements": {
                    "type": "d3m.metadata.hyperparams.Hyperparameter",
                    "default": -1,
                    "structural_type": "int",
                    "semantic_types": []
                },
                "is_configuration": false,
                "min_size": 0
            },
            "exclude_columns": {
                "type": "d3m.metadata.hyperparams.Set",
                "default": [],
                "structural_type": "typing.Sequence[int]",
                "semantic_types": [
                    "https://metadata.datadrivendiscovery.org/types/ControlParameter"
                ],
                "description": "A set of column indices to not operate on. Applicable only if \"use_columns\" is not provided.",
                "elements": {
                    "type": "d3m.metadata.hyperparams.Hyperparameter",
                    "default": -1,
                    "structural_type": "int",
                    "semantic_types": []
                },
                "is_configuration": false,
                "min_size": 0
            }
        },
        "arguments": {
            "hyperparams": {
                "type": "primitives_ubc.smi.semantic_type.Hyperparams",
                "kind": "RUNTIME"
            },
            "volumes": {
                "type": "typing.Union[NoneType, typing.Dict[str, str]]",
                "kind": "RUNTIME",
                "default": null
            },
            "timeout": {
                "type": "typing.Union[NoneType, float]",
                "kind": "RUNTIME",
                "default": null
            },
            "iterations": {
                "type": "typing.Union[NoneType, int]",
                "kind": "RUNTIME",
                "default": null
            },
            "produce_methods": {
                "type": "typing.Sequence[str]",
                "kind": "RUNTIME"
            },
            "inputs": {
                "type": "d3m.container.pandas.DataFrame",
                "kind": "PIPELINE"
            },
            "params": {
                "type": "NoneType",
                "kind": "RUNTIME"
            }
        },
        "class_methods": {},
        "instance_methods": {
            "__init__": {
                "kind": "OTHER",
                "arguments": [
                    "hyperparams",
                    "volumes"
                ],
                "returns": "typing.Any"
            },
            "fit": {
                "kind": "OTHER",
                "arguments": [
                    "timeout",
                    "iterations"
                ],
                "returns": "d3m.primitive_interfaces.base.CallResult[NoneType]",
                "description": "A noop.\n\nParameters\n----------\ntimeout:\n    A maximum time this primitive should be fitting during this method call, in seconds.\niterations:\n    How many of internal iterations should the primitive do.\n\nReturns\n-------\nA ``CallResult`` with ``None`` value."
            },
            "fit_multi_produce": {
                "kind": "OTHER",
                "arguments": [
                    "produce_methods",
                    "inputs",
                    "timeout",
                    "iterations"
                ],
                "returns": "d3m.primitive_interfaces.base.MultiCallResult",
                "description": "A method calling ``fit`` and after that multiple produce methods at once.\n\nParameters\n----------\nproduce_methods:\n    A list of names of produce methods to call.\ninputs:\n    The inputs given to all produce methods.\ntimeout:\n    A maximum time this primitive should take to both fit the primitive and produce outputs\n    for all produce methods listed in ``produce_methods`` argument, in seconds.\niterations:\n    How many of internal iterations should the primitive do for both fitting and producing\n    outputs of all produce methods.\n\nReturns\n-------\nA dict of values for each produce method wrapped inside ``MultiCallResult``."
            },
            "get_params": {
                "kind": "OTHER",
                "arguments": [],
                "returns": "NoneType",
                "description": "A noop.\n\nReturns\n-------\nAn instance of parameters."
            },
            "multi_produce": {
                "kind": "OTHER",
                "arguments": [
                    "produce_methods",
                    "inputs",
                    "timeout",
                    "iterations"
                ],
                "returns": "d3m.primitive_interfaces.base.MultiCallResult",
                "description": "A method calling multiple produce methods at once.\n\nWhen a primitive has multiple produce methods it is common that they might compute the\nsame internal results for same inputs but return different representations of those results.\nIf caller is interested in multiple of those representations, calling multiple produce\nmethods might lead to recomputing same internal results multiple times. To address this,\nthis method allows primitive author to implement an optimized version which computes\ninternal results only once for multiple calls of produce methods, but return those different\nrepresentations.\n\nIf any additional method arguments are added to primitive's produce method(s), they have\nto be added to this method as well. This method should accept an union of all arguments\naccepted by primitive's produce method(s) and then use them accordingly when computing\nresults.\n\nThe default implementation of this method just calls all produce methods listed in\n``produce_methods`` in order and is potentially inefficient.\n\nIf primitive should have been fitted before calling this method, but it has not been,\nprimitive should raise a ``PrimitiveNotFittedError`` exception.\n\nParameters\n----------\nproduce_methods:\n    A list of names of produce methods to call.\ninputs:\n    The inputs given to all produce methods.\ntimeout:\n    A maximum time this primitive should take to produce outputs for all produce methods\n    listed in ``produce_methods`` argument, in seconds.\niterations:\n    How many of internal iterations should the primitive do.\n\nReturns\n-------\nA dict of values for each produce method wrapped inside ``MultiCallResult``."
            },
            "produce": {
                "kind": "PRODUCE",
                "arguments": [
                    "inputs",
                    "timeout",
                    "iterations"
                ],
                "returns": "d3m.primitive_interfaces.base.CallResult[d3m.container.pandas.DataFrame]",
                "singleton": false,
                "inputs_across_samples": [],
                "description": "Returns output dataframe with the structural_type updated in the input metadata\n\nParameters\n----------\ninputs:\n    The inputs of shape [num_inputs, ...].\ntimeout:\n    A maximum time this primitive should take to produce outputs during this method call, in seconds.\niterations:\n    How many of internal iterations should the primitive do.\n\nReturns\n-------\nThe outputs of shape [num_inputs, ...] wrapped inside ``CallResult``."
            },
            "set_params": {
                "kind": "OTHER",
                "arguments": [
                    "params"
                ],
                "returns": "NoneType",
                "description": "A noop.\n\nParameters\n----------\nparams:\n    An instance of parameters."
            },
            "set_training_data": {
                "kind": "OTHER",
                "arguments": [],
                "returns": "NoneType",
                "description": "A noop.\n\nParameters\n----------"
            }
        },
        "class_attributes": {
            "logger": "logging.Logger",
            "metadata": "d3m.metadata.base.PrimitiveMetadata"
        },
        "instance_attributes": {
            "hyperparams": "d3m.metadata.hyperparams.Hyperparams",
            "random_seed": "int",
            "docker_containers": "typing.Dict[str, d3m.primitive_interfaces.base.DockerContainer]",
            "volumes": "typing.Dict[str, str]",
            "temporary_directory": "typing.Union[NoneType, str]"
        }
    },
    "structural_type": "primitives_ubc.smi.semantic_type.SemanticTypeInfer",
    "digest": "5a8423f3930cbeb9dd18594319cfdce018bbb0ffaebb31d7cf8836be90a7b8ba"
}
